package com.abnerrobles.examenandroid.Models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
class DataContainer<T> {
    @JsonProperty("page")
    var page: Int? = 0
    @JsonProperty("results")
    var results: ArrayList<Movies> = ArrayList()
}