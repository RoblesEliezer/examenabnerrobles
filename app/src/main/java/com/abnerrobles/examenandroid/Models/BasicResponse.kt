package com.abnerrobles.examenandroid.Models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
class BasicResponse<T> {
    @JsonProperty("code")
    var code: Int = -1
    @JsonProperty("success")
    var success: Boolean = false
    @JsonProperty("message")
    var message: String = ""
    @JsonProperty("content")
    var content: T? = null
}