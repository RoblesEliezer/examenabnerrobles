package com.abnerrobles.examenandroid.Models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
class Movies {
    @JsonProperty("id")
    var id: Int = 0
    @JsonProperty("title")
    var title: String = ""
    @JsonProperty("overview")
    var overview: String = ""
    @JsonProperty("poster_path")
    var poster_path: String = ""

}