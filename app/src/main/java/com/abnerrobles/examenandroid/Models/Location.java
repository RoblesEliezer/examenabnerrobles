package com.abnerrobles.examenandroid.Models;

public class Location {
    private double latitud,longitud;

    private String time_save;

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getTime_save() {
        return time_save;
    }

    public void setTime_save(String time_save) {
        this.time_save = time_save;
    }
}
