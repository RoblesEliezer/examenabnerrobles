package com.abnerrobles.examenandroid;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.abnerrobles.examenandroid.Fragment.LandingFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, new LandingFragment()).commit();




    }


}