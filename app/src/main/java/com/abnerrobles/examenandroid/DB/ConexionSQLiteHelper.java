package com.abnerrobles.examenandroid.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import com.abnerrobles.examenandroid.Utils.Utils;


public class ConexionSQLiteHelper extends SQLiteOpenHelper {


    public ConexionSQLiteHelper(Context context) {
        super(context, Utils.DATABASE_NAME, null, Utils.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Utils.CREATE_TABLE_MOVIE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+Utils.TABLE_MOVIE);
        onCreate(db);
    }

    public boolean insert_movie(String title,String overview,String poster_path){
        SQLiteDatabase DB = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("title",title);
        contentValues.put("overview",overview);
        contentValues.put("poster_path",poster_path);
        long result=DB.insert(Utils.TABLE_MOVIE,null,contentValues);

        if(result==-1){
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean update_movie(int id,String title,String overview,String poster_path){
        SQLiteDatabase DB = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id",id);
        contentValues.put("title",title);
        contentValues.put("overview",overview);
        contentValues.put("poster_path",poster_path);

        Cursor cursor = DB.rawQuery("SELECT * FROM "+Utils.TABLE_MOVIE+" WHERE name=?",new String[]{title});

        if(cursor.getCount()>0){


        long result=DB.update(Utils.TABLE_MOVIE,contentValues,"title=?",new String[] {title});
        if(result==-1){
            return false;
        }
        else
        {
            return true;
        }
        }else{
            return false;
        }
    }

    public Cursor getData(){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("SELECT * FROM "+Utils.TABLE_MOVIE,null);

        return cursor;
    }



}
