package com.abnerrobles.examenandroid.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.CountDownTimer;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.abnerrobles.examenandroid.Adapter.RecyclerViewAdapterLocation;
import com.abnerrobles.examenandroid.Adapter.RecyclerViewAdapterLocation_;
import com.abnerrobles.examenandroid.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.Context.ALARM_SERVICE;
import static androidx.core.content.ContextCompat.checkSelfPermission;


public class LocationFragment extends Fragment {
    LocationManager locationManager;
    double longitudeGPS, latitudeGPS;
    CountDownTimer countDownTimer;
    boolean tiempo;
    int count = 5*60;
    boolean timerStarted = false;
    private LocationFragment myself;
    private SharedPreferences sharedPreferences;
    private RecyclerView recycler_location;
    private RecyclerViewAdapterLocation_ mAdapter;
    private  FirebaseFirestore cloudFirestore;
    SimpleDateFormat simpleDateFormat;
    String currentDateandTime;




    public LocationFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @SuppressLint("MissingPermission")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_location, container, false);
        sharedPreferences = getActivity().getSharedPreferences("datos_timer", Context.MODE_PRIVATE);
        locationManager = (LocationManager) getActivity().getSystemService(getContext().LOCATION_SERVICE);
        recycler_location = view.findViewById(R.id.recycler_location);
        recycler_location.setLayoutManager(new LinearLayoutManager(getContext()));

        cloudFirestore = FirebaseFirestore.getInstance();

        Query query = cloudFirestore.collection("route_gps");

        FirestoreRecyclerOptions<com.abnerrobles.examenandroid.Models.Location> firestoreRecyclerOptions = new FirestoreRecyclerOptions.Builder<com.abnerrobles.examenandroid.Models.Location>().setQuery(query, com.abnerrobles.examenandroid.Models.Location.class).build();
        mAdapter = new RecyclerViewAdapterLocation_(firestoreRecyclerOptions);
        mAdapter.notifyDataSetChanged();
        recycler_location.setAdapter(mAdapter);


        SetupTimer();
        if(validatePermissions()){
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2 * 20 * 1000, 10, locationListenerGPS);
        }else
        {

        }



        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        mAdapter.stopListening();
    }

    private boolean validatePermissions() {
        if(Build.VERSION.SDK_INT<Build.VERSION_CODES.M){
            return true;
        }
        if ((checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED) && (checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED) )
        {
            return true;
        }
        if(shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) && shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)){
            cargarDialogo();
        }
        else{
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},100);

        }
        return false;
    }
    private void cargarDialogo() {
        androidx.appcompat.app.AlertDialog.Builder dialogo = new androidx.appcompat.app.AlertDialog.Builder(getContext());
        dialogo.setTitle("Permisos Desactivados");
        dialogo.setMessage("Debes aceptar los permisos");
        dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},100);
            }
        });
        dialogo.show();
    }
    private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    public void GPSUpdate() {
        if (!checkLocation())
            return;
        locationManager.removeUpdates(locationListenerGPS);

    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle("Activa GPS")
                .setMessage("Su ubicación esta desactivada.\npor favor active su ubicación " +
                        "usa esta app")
                .setPositiveButton("Configuración de ubicación", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private final LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitudeGPS = location.getLongitude();
            latitudeGPS = location.getLatitude();
          /* getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //longitudeValueGPS.setText(longitudeGPS + "");
                    //latitudeValueGPS.setText(latitudeGPS + "");


                    //Toast.makeText(MainActivity.this, "GPS Provider update", Toast.LENGTH_SHORT).show();


                }
            });*/

        }
        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
        }
        @Override
        public void onProviderDisabled(String s) {
        }
    };

    @Override
    public void onDestroy() {
        try {
            countDownTimer.cancel();
        } catch (Exception e){}
        super.onDestroy();
    }

    void SetupTimer() {
        myself = this;
        tiempo = false;
        count = sharedPreferences.getInt("count", 5*60);
        sharedPreferences.edit().putInt("count",10*60).apply();
        if(count < 10*60) {
            long seconds = count;
            if(timerStarted == false) {
                timerStarted = true;
                startTimer(count);
            }
        }
    }

    private void startTimer(final int seconds) {
        countDownTimer = new CountDownTimer(seconds * 1000, 500) {
            @Override
            public void onTick(long leftTimeInMilliseconds) {
                long seconds = leftTimeInMilliseconds / 1000;
                myself.count = (int)seconds;
                myself.sharedPreferences.edit().putInt("count",myself.count).apply();
            }
            @Override
            public void onFinish() {
                if(myself.count==0){
                    tiempo = true;
                    try {
                        try {

                                //Se actualiza la ubicacion y se tiene que regresar al Timer a 5 minutos
                               GPSUpdate();
                               SetupTimer();
                               updateList_CloudFireStore();
                               setAlarm();


                        } catch (Exception e) {
                        }

                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    private void updateList_CloudFireStore() {
        cloudFirestore = FirebaseFirestore.getInstance();
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        currentDateandTime = simpleDateFormat.format(new Date());

        Map<String,Object> map = new HashMap<>();
        map.put("latitud",latitudeGPS);
        map.put("longitud",longitudeGPS);
        map.put("time_save",currentDateandTime);
        cloudFirestore.document("route_gps").set(map);
    }


    public void setAlarm(){
        NotificationManager notificationManager = (NotificationManager)getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "location";
        CharSequence name = "Alerta";
        String description = "Se volvieron a actualizar las coordenadas GPS";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notificaciones", NotificationManager.IMPORTANCE_MAX);
            notificationChannel.setDescription("Se volvieron a actualizar las coordenadas GPS");
            notificationChannel.enableLights(true);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getContext(), NOTIFICATION_CHANNEL_ID);
        notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker("¡Alerta!")
                .setContentTitle(name)
                .setContentText(description);
        notificationManager.notify(1, notificationBuilder.build());


    }

}