package com.abnerrobles.examenandroid.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.abnerrobles.examenandroid.Adapter.RecyclerViewAdapterMovie;
import com.abnerrobles.examenandroid.Adapter.RecyclerViewAdapterMovie_;
import com.abnerrobles.examenandroid.DB.ConexionSQLiteHelper;
import com.abnerrobles.examenandroid.Models.DataContainer;
import com.abnerrobles.examenandroid.Models.Movies;
import com.abnerrobles.examenandroid.R;
import com.abnerrobles.examenandroid.Services.MoviesEndpoint;
import com.abnerrobles.examenandroid.Services.ServiceGenerator;
import com.abnerrobles.examenandroid.Utils.ExpandableHeightListView;
import com.abnerrobles.examenandroid.Utils.Utils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;


public class MovieFragment extends Fragment {
    private MoviesEndpoint moviesEndpoint;
    private ArrayList<Movies> movie_list_offline;
    ConexionSQLiteHelper conn;
    RecyclerViewAdapterMovie_ recyclerViewAdapterMovie;
    RecyclerView recyclerView;
     LinearLayoutManager linearLayoutManager;
     SharedPreferences sharedPreferences;
     private ConnectivityManager connectivityManager;
     private boolean set_list_movie;
     private NetworkInfo networkInfo;


    public MovieFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view =inflater.inflate(R.layout.fragment_movie, container, false);
       recyclerView = view.findViewById(R.id.recycler_movie);
       recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        movie_list_offline = new ArrayList<>();
        conn = new ConexionSQLiteHelper(getContext());

        if(validarConexion()){
            Log.d("Abner","Connection ok");
            getMovies();
        }else{
            Log.d("Abner","Connection bad");
            getMovies_offline();
        }






       return view;
    }



    private Boolean validarConexion() {
        //Conexion offline
        sharedPreferences = getActivity().getSharedPreferences("session", Context.MODE_PRIVATE);
        set_list_movie = sharedPreferences.getBoolean("set_list_movie",false);
        connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
           return false;
        }
    }

    public void getMovies(){
        Retrofit builder = new Retrofit.Builder()
                .baseUrl(ServiceGenerator.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MoviesEndpoint moviesEndpoint = builder.create(MoviesEndpoint.class);

        Call<DataContainer<ArrayList<Movies>>> call = moviesEndpoint.getMovies();

        call.enqueue(new Callback<DataContainer<ArrayList<Movies>>>() {
            @Override
            public void onResponse(Call<DataContainer<ArrayList<Movies>>> call, Response<DataContainer<ArrayList<Movies>>> response) {
                if(!response.isSuccessful()){
                    Log.d("Abner","Code "+response.code());
                    return;
                }


                Log.d("Abner","Llega la lista"+response.body().getResults()); //Esta llegando la lista con datos del servicio


                recyclerViewAdapterMovie = new RecyclerViewAdapterMovie_(response.body().getResults(),getContext());
                recyclerView.setAdapter(recyclerViewAdapterMovie);
                recyclerViewAdapterMovie.setList(response.body().getResults());
                recyclerViewAdapterMovie.notifyDataSetChanged();



                //Aqui se valida si ya se lleno la base de datos de SQLITE
                if(set_list_movie==false){
                    for(int i=0;i<response.body().getResults().size();i++){
                        conn.insert_movie(response.body().getResults().get(i).getTitle(),response.body().getResults().get(i).getOverview(),response.body().getResults().get(i).getPoster_path());
                    }
                    sharedPreferences.edit().putBoolean("set_list_movie",true);
                }else{
                   //Ya no se requiere volver a llenar la base de datos
                }

            }

            @Override
            public void onFailure(Call<DataContainer<ArrayList<Movies>>> call, Throwable t) {
                Log.d("Abner","Error "+t.getMessage());
            }
        });




    }

    private void getMovies_offline() {
        SQLiteDatabase db = conn.getReadableDatabase();
        Movies movies = new Movies();
        Cursor cursor = db.rawQuery("SELECT "+Utils.ID_MOVIE+","+Utils.TITLE+","+Utils.OVERVIEW+" FROM "+ Utils.TABLE_MOVIE,null);
        while (cursor.moveToNext()){
            movies.setId(cursor.getInt(0));
            movies.setTitle(cursor.getString(1));
            movies.setOverview(cursor.getString(2));
            movie_list_offline.add(movies);
        }



        recyclerViewAdapterMovie = new RecyclerViewAdapterMovie_(movie_list_offline,getContext());
        recyclerView.setAdapter(recyclerViewAdapterMovie);
        recyclerViewAdapterMovie.setList(movie_list_offline);
        recyclerViewAdapterMovie.notifyDataSetChanged();
    }
}