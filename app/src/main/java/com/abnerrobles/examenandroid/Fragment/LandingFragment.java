package com.abnerrobles.examenandroid.Fragment;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.abnerrobles.examenandroid.MainActivity;
import com.abnerrobles.examenandroid.R;

import static androidx.core.content.ContextCompat.checkSelfPermission;


public class LandingFragment extends Fragment {
    private CardView btn_fragment_foto,btn_fragment_ubicacion,btn_fragment_movie;



    CountDownTimer countDownTimer;
    boolean tiempo;
    int count = 5*60;
    boolean timerStarted = false;
    private LandingFragment myself;
    private SharedPreferences sharedPreferences;
    public LandingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_landing, container, false);
        btn_fragment_foto = view.findViewById(R.id.btn_fragment_foto);
        btn_fragment_ubicacion = view.findViewById(R.id.btn_fragment_ubicacion);
        btn_fragment_movie = view.findViewById(R.id.btn_fragment_movie);
        sharedPreferences = getActivity().getSharedPreferences("datos_timer", Context.MODE_PRIVATE);

        buttons();
        SetupTimer();
        validatePermissions();
        return view;
    }

    private void buttons() {
        btn_fragment_foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, new FotoFragment()).addToBackStack(null).commit();
            }
        });
        btn_fragment_ubicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, new LocationFragment()).addToBackStack(null).commit();
            }
        });
        btn_fragment_movie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, new MovieFragment()).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public void onDestroy() {
        try {
            countDownTimer.cancel();
        } catch (Exception e){}
        super.onDestroy();
    }

    void SetupTimer() {
        myself = this;
        tiempo = false;
        count = sharedPreferences.getInt("count", 5*60);
        sharedPreferences.edit().putInt("count",10*60).apply();
        if(count < 10*60) {
            long seconds = count;
            if(timerStarted == false) {
                timerStarted = true;
                startTimer(count);
            }
        }
    }

    private void startTimer(final int seconds) {
        countDownTimer = new CountDownTimer(seconds * 1000, 500) {
            @Override
            public void onTick(long leftTimeInMilliseconds) {
                long seconds = leftTimeInMilliseconds / 1000;
                myself.count = (int)seconds;
                myself.sharedPreferences.edit().putInt("count",myself.count).apply();
            }
            @Override
            public void onFinish() {
                if(myself.count==0){
                    tiempo = true;
                    try {
                        try {
                            if(myself.isVisible()){
                                //Se actualiza la ubicacion y se tiene que regresar al Timer a 5 minutos
                            }

                        } catch (Exception e) {
                        }

                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    private boolean validatePermissions() {
        if(Build.VERSION.SDK_INT<Build.VERSION_CODES.M){
            return true;
        }
        if ((checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED) && (checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED) )
        {
            return true;
        }
        if(shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) && shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)){
            cargarDialogo();
        }
        else{
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},100);

        }
        return false;
    }

    private void cargarDialogo() {
        androidx.appcompat.app.AlertDialog.Builder dialogo = new androidx.appcompat.app.AlertDialog.Builder(getContext());
        dialogo.setTitle("Permisos Desactivados");
        dialogo.setMessage("Debes aceptar los permisos");
        dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},100);
            }
        });
        dialogo.show();
    }
}