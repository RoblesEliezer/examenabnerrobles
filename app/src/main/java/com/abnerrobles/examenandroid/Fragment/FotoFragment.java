package com.abnerrobles.examenandroid.Fragment;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.abnerrobles.examenandroid.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.opensooq.supernova.gligar.GligarPicker;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static androidx.core.content.ContextCompat.checkSelfPermission;


public class FotoFragment extends Fragment {
    private CardView btn_camera;
    private StorageReference folder;
    private DatabaseReference myRef;
    private FirebaseDatabase database;
    private static final int Image_Capture_code =1;
    static final int GALERY_IMAGE_CAPTURE_CODE = 4;
    private Uri imageUri;
    private Bitmap bp;

    public FotoFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_foto, container, false);
       btn_camera = view.findViewById(R.id.btn_camera);
       folder = FirebaseStorage.getInstance().getReference().child("imagenes");
       database = FirebaseDatabase.getInstance();
       myRef = database.getReference().child("User");


        buttons();

       return view;
    }

    private void buttons() {
        btn_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] options = {"Galería", "Cámara"};
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
                builder.setTitle("Elige una opción");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0:
                                if(validaPermisos()){
                                    abrirGaleria();
                                }
                                else{}
                                break;
                            case 1:
                                if(validaPermisos()){
                                    abrirCamara();
                                }
                                else{}
                                break;
                            default: Toast.makeText(getContext(), "Invalid", Toast.LENGTH_SHORT).show(); break;
                        }
                    }
                });
                builder.show();


            }
        });

    }

    private void abrirGaleria() {
        Intent galery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        galery.setType("image/*");
        if(galery.resolveActivity(getActivity().getPackageManager()) != null){
            startActivityForResult(galery,GALERY_IMAGE_CAPTURE_CODE);
        }
    }

    public void abrirCamara(){
        Intent cInt = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(cInt.resolveActivity(getActivity().getPackageManager()) != null){
            startActivityForResult(cInt,Image_Capture_code);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Image_Capture_code) {
            if (resultCode == Activity.RESULT_OK) {
                bp = (Bitmap) data.getExtras().get("data");
                String path = MediaStore.Images.Media.insertImage(getContext().getContentResolver(), bp,
                                null, null);
                imageUri =  Uri.parse(path);
                folder = FirebaseStorage.getInstance().getReference().child("imagenes");
                final StorageReference file_name = folder.child("file"+imageUri.getLastPathSegment());
                file_name.putFile(imageUri).addOnSuccessListener(taskSnapshot -> file_name.getDownloadUrl().addOnSuccessListener(uri -> {
                    HashMap<String,String> hashMap = new HashMap<>();
                    hashMap.put("link",String.valueOf(imageUri));
                    myRef.setValue(hashMap);
                    Toast.makeText(getContext(), "Imagen cargada correctamente en FirebaseStorage", Toast.LENGTH_SHORT).show();
                }));
            } else if (resultCode == Activity.RESULT_CANCELED) {
                //  Toast.makeText(getActivity(), "Cancelled", Toast.LENGTH_LONG).show();
            }
        }else if(requestCode == GALERY_IMAGE_CAPTURE_CODE){
            if (resultCode == Activity.RESULT_OK) {
                imageUri = data.getData();
                folder = FirebaseStorage.getInstance().getReference().child("imagenes");
                final StorageReference file_name = folder.child("file"+imageUri.getLastPathSegment());
                file_name.putFile(imageUri).addOnSuccessListener(taskSnapshot -> file_name.getDownloadUrl().addOnSuccessListener(uri -> {
                    HashMap<String,String> hashMap = new HashMap<>();
                    hashMap.put("link",String.valueOf(imageUri));
                    myRef.setValue(hashMap);
                    Toast.makeText(getContext(), "Imagen cargada correctamente en FirebaseStorage", Toast.LENGTH_SHORT).show();

                }));
            } else if (resultCode == Activity.RESULT_CANCELED) {
                //  Toast.makeText(getActivity(), "Cancelled", Toast.LENGTH_LONG).show();
            }
        }

    }

    private boolean validaPermisos() {
        if(Build.VERSION.SDK_INT<Build.VERSION_CODES.M){
            return true;
        }
        if ((checkSelfPermission(getContext(), Manifest.permission.CAMERA)== PackageManager.PERMISSION_GRANTED))
        {
            return true;
        }
        if(shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)){
            cargarDialogo();
        }
        else{
            requestPermissions(new String[]{Manifest.permission.CAMERA},100);

        }
        return false;
    }



    private void cargarDialogo() {
        AlertDialog.Builder dialogo = new AlertDialog.Builder(getContext());
        dialogo.setTitle("Permisos Desactivados");
        dialogo.setMessage("Debes aceptar los permisos");
        dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},100);
            }
        });
        dialogo.show();
    }
}