package com.abnerrobles.examenandroid.Utils;

public class Utils {
    public static final String DATABASE_NAME = "movie.db";
    public static final int DATABASE_VERSION = 1;

    //Variables movie
    public static final String TABLE_MOVIE = "movie";
    public static final String ID_MOVIE = "id";
    public static final String TITLE = "title";
    public static final String OVERVIEW = "overview";
    public static final String POSTER_PATH = "poster_path";

    //Create table

    public static final String CREATE_TABLE_MOVIE = "CREATE TABLE " + TABLE_MOVIE+"("
            +ID_MOVIE+" INTEGER PRIMARY KEY AUTOINCREMENT,"
            +TITLE+" TEXT, "
            +OVERVIEW+" TEXT, "
            +POSTER_PATH+" TEXT)";
}
