package com.abnerrobles.examenandroid.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.abnerrobles.examenandroid.Models.Movies;
import com.abnerrobles.examenandroid.R;

import java.util.ArrayList;

public class RecyclerViewAdapterMovie_ extends RecyclerView.Adapter<RecyclerViewAdapterMovie_.MovieViewHolder> {
    ArrayList<Movies> movies_list;
   public Context context;
    View mainLayout;
    public RecyclerViewAdapterMovie_(ArrayList<Movies> movies_list_, Context context){
        this.context = context;
        this.movies_list = movies_list_;
    }




    public static class MovieViewHolder extends RecyclerView.ViewHolder {
        Context context;

        private TextView id,title,description;
        MovieViewHolder(View itemView) {
            super(itemView);
            id = (TextView)itemView.findViewById(R.id.id);
            title = (TextView)itemView.findViewById(R.id.title_movie);
            description = (TextView)itemView.findViewById(R.id.description);
        }
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);
        MovieViewHolder movieViewHolder = new MovieViewHolder(v);
        return movieViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {

        holder.id.setText(""+movies_list.get(position).getId());
        holder.title.setText(movies_list.get(position).getTitle());
        holder.description.setText(movies_list.get(position).getOverview());


    }

    public void setList(ArrayList<Movies> list){
        this.movies_list = list;
    }


    @Override
    public int getItemCount() {
        return movies_list.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
