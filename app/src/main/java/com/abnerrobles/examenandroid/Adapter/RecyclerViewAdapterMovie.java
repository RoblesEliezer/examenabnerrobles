package com.abnerrobles.examenandroid.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.abnerrobles.examenandroid.Models.Location;
import com.abnerrobles.examenandroid.Models.Movies;
import com.abnerrobles.examenandroid.R;
import com.abnerrobles.examenandroid.Services.ServiceGenerator;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RecyclerViewAdapterMovie extends  RecyclerView.Adapter<RecyclerViewAdapterMovie.MyViewHolder> {
    private Context mContext;
    private ArrayList<Movies> movie_list;



    public RecyclerViewAdapterMovie(ArrayList<Movies> dataset, Context context){
        this.movie_list = dataset;
        this.mContext = context;
    }

    public void setList(ArrayList<Movies> list){
        this.movie_list = list;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder{

        View view;
        private ImageView icon_movie;
        private TextView id,title,description;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            icon_movie = itemView.findViewById(R.id.icon_movie);
            id = itemView.findViewById(R.id.id);
            title = itemView.findViewById(R.id.title_movie);
            description = itemView.findViewById(R.id.description);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        View view = holder.view;
        Movies dataModel = movie_list.get(position);
        String path_photo = dataModel.getPoster_path();
       // String url_final_photo = ServiceGenerator.API_BASE_URL+"movie/"+dataModel.getId().toString()+"/images/"+ServiceGenerator.API_KEY;

       // Picasso.get().load(url_final_photo).into(icon_movie);
        holder.id.setText("Id: "+dataModel.getId());
        holder.title.setText("Título: "+dataModel.getTitle());
        holder.description.setText("Descripción: "+dataModel.getOverview());




    }

    @Override
    public int getItemCount() {
        return 0;
    }

    @Override
    public void onViewAttachedToWindow(@NonNull MyViewHolder holder) {
        super.onViewAttachedToWindow(holder);
    }

}
