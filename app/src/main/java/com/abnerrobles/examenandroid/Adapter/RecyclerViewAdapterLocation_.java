package com.abnerrobles.examenandroid.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.abnerrobles.examenandroid.Models.Location;

import com.abnerrobles.examenandroid.R;
import com.firebase.ui.firestore.*;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class RecyclerViewAdapterLocation_ extends FirestoreRecyclerAdapter<Location, RecyclerViewAdapterLocation_.ViewHolder> {


    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
     * FirestoreRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public RecyclerViewAdapterLocation_(FirestoreRecyclerOptions<Location> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder viewHolder, int i, @NonNull Location location) {
        viewHolder.longitud.setText(""+location.getLongitud());
        viewHolder.latitud.setText(""+location.getLatitud());
        viewHolder.save_time.setText(""+location.getTime_save());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_location,null);

        return new ViewHolder(view);
    }



    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView latitud,longitud,save_time;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            latitud = itemView.findViewById(R.id.latitud);
            longitud = itemView.findViewById(R.id.longitud);
            save_time = itemView.findViewById(R.id.time_save);

        }
    }
}
