package com.abnerrobles.examenandroid.Adapter;

import android.content.Context;
import android.location.LocationListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.abnerrobles.examenandroid.Models.Location;
import com.abnerrobles.examenandroid.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class RecyclerViewAdapterLocation extends  RecyclerView.Adapter<RecyclerViewAdapterLocation.MyViewHolder> {
    private Context mContext;
    private ArrayList<Location> location_list;
    private MapView mapView;
    private GoogleMap map;
    float ZOOM_MAP = 17.0f;
    private TextView latitud,longitud,hora_actualizacion;

    public RecyclerViewAdapterLocation(ArrayList<Location> dataset, Context context){
        this.location_list = dataset;
        this.mContext = context;
    }



    public static class MyViewHolder extends RecyclerView.ViewHolder{

        View view;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.view=itemView;
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_location,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        View view = holder.view;



        Location dataModel = location_list.get(position);
        //mapView =(MapView) view.findViewById(R.id.mapview);
        latitud = view.findViewById(R.id.latitud);
        longitud = view.findViewById(R.id.longitud);
        hora_actualizacion= view.findViewById(R.id.time_save);



        latitud.setText("Latitud: "+dataModel.getLatitud());
        longitud.setText("Longitud: "+dataModel.getLongitud());
        hora_actualizacion.setText("Hora actualización: "+dataModel.getTime_save());

        LatLng latLng = new LatLng(dataModel.getLatitud(),dataModel.getLongitud());
        CameraUpdate myLocation = CameraUpdateFactory.newLatLngZoom(latLng, ZOOM_MAP);
        map.animateCamera(myLocation);




        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                CameraUpdate myLocation = CameraUpdateFactory.newLatLngZoom(latLng, ZOOM_MAP);
                googleMap.animateCamera(myLocation);
            }
        });


    }

    @Override
    public int getItemCount() {
        return 0;
    }

    @Override
    public void onViewAttachedToWindow(@NonNull MyViewHolder holder) {
        super.onViewAttachedToWindow(holder);
    }


}
