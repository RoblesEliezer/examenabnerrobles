package com.abnerrobles.examenandroid.Services;

import android.content.Context;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class ServiceGenerator {
    public static final String API_BASE_URL = "https://api.themoviedb.org/3/";
    public static final String API_KEY = "?api_key=1bbb895b064f99750c26c7cade26f6b1";}
