package com.abnerrobles.examenandroid.Services;

import com.abnerrobles.examenandroid.Models.BasicResponse;
import com.abnerrobles.examenandroid.Models.DataContainer;
import com.abnerrobles.examenandroid.Models.Movies;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MoviesEndpoint {
    @GET("movie/top_rated"+ServiceGenerator.API_KEY)
    Call<DataContainer<ArrayList<Movies>>> getMovies();
}
